import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UrlsComponent } from "./features/urls/urls.component";
import { UrlListComponent } from "./features/urls/url-list/url-list.component";
import { UrlStartComponent } from "./features/urls/url-start/url-start.component";
import { UrlItemComponent } from "./features/urls/url-list/url-item/url-item.component";
import { UrlEditComponent } from "./features/urls/url-edit/url-edit.component";
import { UrlDetailComponent } from "./features/urls/url-detail/url-detail.component";
import { AuthComponent } from "./features/auth/auth.component";
import { UrlService } from "./features/urls/url.service";
import { AuthInterceptorService } from "./features/auth/auth-interceptor.service";
import { HeaderComponent } from "./layout/header/header.component";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MatPaginatorModule } from "@angular/material/paginator";
import { environment } from "src/environments/environment";

@NgModule({
  declarations: [
    AppComponent,
    UrlsComponent,
    UrlListComponent,
    UrlStartComponent,
    UrlItemComponent,
    UrlEditComponent,
    UrlDetailComponent,
    AuthComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatPaginatorModule,
  ],
  providers: [
    UrlService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    },
    { provide: "BASE_API_URL", useValue: environment.apiUrl },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
