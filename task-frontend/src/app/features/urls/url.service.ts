import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Url } from "./url.model";
import { catchError, retry } from "rxjs/operators";

@Injectable()
export class UrlService {
  urlsChanged = new Subject<Url[]>();

  public API_URL = "http://localhost:3000/urls";

  private urls: Url[] = [];
  constructor(private http: HttpClient) {}

  setURLs(urls: Url[]) {
    this.urls = urls;
    this.urlsChanged.next(this.urls.slice());
  }

  getURLs() {
    return this.urls.slice();
  }

  getURL(index: number) {
    return this.urls[index];
  }

  addURL(url: Url) {
    let originalURL: string = url.originalURL;
    const result = this.http
      .post<Url>(this.API_URL, { originalURL })
      .subscribe();

    this.urls.push(url);
    this.urlsChanged.next(this.urls.slice());

    return result;
  }

  updateURL(index: number, newUrl: Url): void {
    this.urls[index] = newUrl;
    this.urlsChanged.next(this.urls.slice());
  }

  deleteURL(index: number) {
    return this.http
      .delete(`${this.API_URL}/${this.urls[index].id}`)
      .subscribe((response) => {
        if (response === null) {
          this.urls.splice(index, 1);
          this.urlsChanged.next(this.urls.slice());
        }
      });
  }

  findById(shortenedUrl: string): Observable<Url> {
    return this.http
      .get<Url>(this.API_URL + "/short/" + shortenedUrl)
      .pipe(retry(1), catchError(this.handleError));
  }

  private handleError(error: any): Promise<any> {
    console.error("An error occurred", error);
    return Promise.reject(error.message || error);
  }
}
