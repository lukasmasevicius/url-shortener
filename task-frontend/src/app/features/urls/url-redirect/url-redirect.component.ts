import { Component, OnInit } from "@angular/core";
import { UrlService } from "../url.service";
import { ActivatedRoute, ParamMap, Params } from "@angular/router";
import { switchMap } from "rxjs/operators";

@Component({
  selector: "app-url-redirect",
  templateUrl: "./url-redirect.component.html",
  styleUrls: ["./url-redirect.component.css"],
})
export class UrlRedirectComponent implements OnInit {
  public redirectToUrl!: string;
  constructor(private service: UrlService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.service.findById(params["id"]).subscribe((res) => {
        this.redirectToUrl = res.originalURL;
        window.location.assign(this.redirectToUrl);
      });
    });
  }
}
