import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { FormGroup, FormControl, FormArray, Validators } from "@angular/forms";

import { UrlService } from "../url.service";

@Component({
  selector: "app-url-edit",
  templateUrl: "./url-edit.component.html",
  styleUrls: ["./url-edit.component.css"],
})
export class UrlEditComponent implements OnInit {
  id: number = 0;
  editMode: boolean = false;
  urlForm!: FormGroup;
  success: boolean = false;

  constructor(
    private _route: ActivatedRoute,
    private _urlService: UrlService,
    private _router: Router
  ) {}

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
      this.id = +params["id"];
      this.editMode = params["id"] != null;
      this.initForm();
    });
  }

  onSubmit(): void {
    this._urlService.addURL(this.urlForm?.value);
    this.success = true;
    this.urlForm.reset();
  }

  onCloseSuccess(): void {
    this.success = false;
    this.onCancel();
  }

  onCancel(): void {
    this._router.navigate(["../"], { relativeTo: this._route });
  }

  private initForm(): void {
    let originalURL = "";
    const urlRegex =
      /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

    this.urlForm = new FormGroup({
      originalURL: new FormControl(originalURL, [
        Validators.required,
        Validators.pattern(urlRegex),
      ]),
    });
  }
}
