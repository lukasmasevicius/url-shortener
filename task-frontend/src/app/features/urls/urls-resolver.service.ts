import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { DataStorageService } from "../../shared/data-storage.service";
import { Url } from "./url.model";

@Injectable({ providedIn: "root" })
export class UrlsResolverService implements Resolve<Url[]> {
  constructor(private dataStorageService: DataStorageService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.dataStorageService.fetchURLs();
  }
}
