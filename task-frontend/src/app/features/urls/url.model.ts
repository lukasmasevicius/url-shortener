export class Url {
  public id: string;
  public originalURL: string;
  public shortenedURL: string;

  constructor(id: string, originalURL: string, shortenedURL: string) {
    this.id = id;
    this.originalURL = originalURL;
    this.shortenedURL = shortenedURL;
  }
}
