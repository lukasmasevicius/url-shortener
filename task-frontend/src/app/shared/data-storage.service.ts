import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map, tap, take, exhaustMap } from "rxjs/operators";

import { AuthService } from "../features/auth/auth.service";
import { Url } from "../features/urls/url.model";
import { UrlService } from "../features/urls/url.service";

@Injectable({ providedIn: "root" })
export class DataStorageService {
  constructor(
    private http: HttpClient,
    private urlService: UrlService,
    private authService: AuthService
  ) {}

  fetchURLs() {
    const result = this.http.get<Url[]>("http://localhost:3000/urls").pipe(
      map((urls) => {
        return urls.map((url) => {
          return {
            ...url,
          };
        });
      }),
      tap((url) => {
        this.urlService.setURLs(url);
      })
    );

    return result;
  }
}
