import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from "./features/auth/auth.guard";
import { UrlDetailComponent } from "./features/urls/url-detail/url-detail.component";
import { UrlEditComponent } from "./features/urls/url-edit/url-edit.component";
import { UrlStartComponent } from "./features/urls/url-start/url-start.component";
import { UrlsComponent } from "./features/urls/urls.component";
import { UrlsResolverService } from "./features/urls/urls-resolver.service";
import { AuthComponent } from "./features/auth/auth.component";
import { UrlRedirectComponent } from "./features/urls/url-redirect/url-redirect.component";

const routes: Routes = [
  { path: "", redirectTo: "/urls", pathMatch: "full" },

  {
    path: "urls",
    component: UrlsComponent,
    canActivate: [AuthGuard],
    resolve: [UrlsResolverService],
    runGuardsAndResolvers: "always",
    children: [
      { path: "", component: UrlStartComponent },
      {
        path: "new",
        component: UrlEditComponent,
        runGuardsAndResolvers: "always",
      },
      {
        path: ":id",
        component: UrlDetailComponent,
        resolve: [UrlsResolverService],
        runGuardsAndResolvers: "always",
      },
    ],
  },

  { path: "auth", component: AuthComponent },
  { path: ":id", component: UrlRedirectComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
