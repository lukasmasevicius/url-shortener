import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UrlsModule } from "./urls/urls.module";
import { AuthModule } from "./auth/auth.module";
import { UsersModule } from "./users/users.module";

@Module({
  imports: [
    UrlsModule,
    MongooseModule.forRoot(
      "mongodb+srv://db_admin:t1uIj6nFee5xPUty@cluster0.k4vg5.mongodb.net/urlsShortener?retryWrites=true&w=majority"
    ),
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
