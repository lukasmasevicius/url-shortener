import { IsEmail, IsString, IsNotEmpty, MinLength } from "class-validator";

export class UserDto {
  readonly _id: number;

  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  @MinLength(6)
  readonly password: string;
}
