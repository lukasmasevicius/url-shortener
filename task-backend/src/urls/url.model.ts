import * as mongoose from "mongoose";

export const URLSchema = new mongoose.Schema({
  originalURL: { type: String, required: true },
  shortenedURL: { type: String, required: true },
});

export interface URL extends mongoose.Document {
  id: string;
  originalURL: string;
  shortenedURL: string;
}
