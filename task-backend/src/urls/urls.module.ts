import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { URLSchema } from "./url.model";

import { URLsController } from "./urls.controller";
import { URLsService } from "./urls.service";

@Module({
  imports: [MongooseModule.forFeature([{ name: "URL", schema: URLSchema }])],
  controllers: [URLsController],
  providers: [URLsService],
})
export class UrlsModule {}
