import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { URL } from "./url.model";

@Injectable()
export class URLsService {
  constructor(@InjectModel("URL") private readonly urlModel: Model<URL>) {}
  async insertURL(originalURL: string) {
    let shortenedKey = "";

    // #This task is partially accomplished, MD5 hash was used for one-way hashing.
    const crypto = require("crypto");
    const hash = crypto.createHash("md5").update(originalURL).digest("hex");
    const charactersLength = hash.length;

    // #To prevent duplicates 7 random chars are picked from hashed string
    for (let i = 0; i < 7; i++) {
      shortenedKey += hash.charAt(Math.floor(Math.random() * charactersLength));
    }
    const newURL = new this.urlModel({
      originalURL: originalURL,
      shortenedURL: shortenedKey,
    });
    const result = await newURL.save();

    return result;
  }

  async generateURL(originalURL: string) {
    const newURL = new this.urlModel({
      originalURL: originalURL,
    });
    const result = await newURL.save();

    return result.id as string;
  }

  async getURLs() {
    const urls = await this.urlModel.find().exec();
    return urls.map((url) => ({
      id: url.id,
      originalURL: url.originalURL,
      shortenedURL: url.shortenedURL,
    }));
  }

  async getSingleURL(urlId: string) {
    const url = await this.findURL(urlId);
    return url;
  }

  async getShortenedURL(shortenURL: string) {
    const url = await this.findLongURL(shortenURL);
    return url;
  }

  async deleteURL(urlId: string) {
    console.log(urlId);
    const result = await this.urlModel.deleteOne({ _id: urlId });
    if (result.n === 0) {
      throw new NotFoundException("Could not find url.");
    }
    return result;
  }

  private async findURL(id: string): Promise<URL> {
    let url;
    try {
      url = await this.urlModel.findById(id);
    } catch (error) {
      throw new NotFoundException("Could not find url.");
    }
    if (!url) {
      throw new NotFoundException("Could not find url.");
    }
    return url;
  }

  private async findLongURL(shortenedURL: string): Promise<URL> {
    let url;
    try {
      url = await this.urlModel.findOne({ shortenedURL: shortenedURL });
    } catch (error) {
      throw new NotFoundException("Could not find Original url.");
    }
    if (!url) {
      throw new NotFoundException("Could not find Original url.");
    }
    return url;
  }
}
