import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Delete,
  UseGuards,
} from "@nestjs/common";

import { URLsService } from "./urls.service";
import { AuthGuard } from "@nestjs/passport";

@Controller("urls")
export class URLsController {
  constructor(private readonly urlsService: URLsService) {}
  public redirectToUrl: string;

  @UseGuards(AuthGuard("jwt"))
  @Post()
  async addURL(@Body("originalURL") originalURL: string) {
    const generatedId = await this.urlsService.insertURL(originalURL);
    return { id: generatedId };
  }

  @UseGuards(AuthGuard("jwt"))
  @Post()
  async generateURL(@Body("originalURL") originalURL: string) {
    const generatedId = await this.urlsService.generateURL(originalURL);
    return { id: generatedId };
  }

  @UseGuards(AuthGuard("jwt"))
  @Get()
  async getAllURLs() {
    const urls = await this.urlsService.getURLs();
    return urls;
  }

  @UseGuards(AuthGuard("jwt"))
  @Get(":id")
  getURL(@Param("id") urlId: string) {
    return this.urlsService.getSingleURL(urlId);
  }

  @Get("short/:short_id")
  navigateURL(
    @Param("short_id") urlId: string
    // @Response() response: express.Response,
  ) {
    return this.urlsService.getShortenedURL(urlId);
    // @ Browser reacts faster about the redirections than Angular
    //    Had to store as normal request

    // this.urlsService.getShortenURL(urlId).then((res) => {
    //   this.redirectToUrl = res;
    // });
    // if (this.redirectToUrl) {
    //   return response.redirect(301, this.redirectToUrl);
    // } else return null;
  }

  @UseGuards(AuthGuard("jwt"))
  @Delete(":id")
  async removeURL(@Param("id") urlId: string) {
    await this.urlsService.deleteURL(urlId);
    return null;
  }
}
