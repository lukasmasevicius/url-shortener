import { Injectable } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { JwtService } from "@nestjs/jwt";
import { jwtConstants } from "./constants";
import * as bcrypt from "bcryptjs";

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(email);
    const password = await bcrypt.compare(pass, user.password);

    if (user && password) {
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = { email: user._doc.email, sub: user._doc._id };
    const expiresIn = jwtConstants.expiresIn.match(/\d/g).join("");
    return {
      access_token: this.jwtService.sign(payload),
      userId: user._doc._id,
      email: user._doc.email,
      expiresIn: expiresIn,
    };
  }
}
